<?php

namespace App\Models;



class Post 
{
    private static $blog_posts = [
        [
            "title" => "judul post pertama",
            "slug" => "judul-post-pertama",
            "autor" => "SMK TI BALI GLOBAL JIMBARAN",
            "body" => "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ea eius, rem debitis ad magnam id natus tempora quasi, eum, voluptas quidem. Incidunt officiis expedita ipsa voluptatum mollitia porro aliquam ab soluta nemo tempora? Officia nulla adipisci eius in aut accusamus facilis. Maiores explicabo numquam sit dolore. Culpa adipisci, sunt nihil dignissimos unde cumque impedit excepturi exercitationem eligendi vel, aut sint modi alias illum laborum consequuntur eos incidunt atque. Alias minus maiores velit omnis rem, maxime in adipisci cum vitae minima?"
        ],
        [
            "title" => "judul post kedua",
            "slug" => "judul-post-kedua",
            "autor" => "SMK TI BALI GLOBAL JIMBARAN",
            "body" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore iste nihil maxime at id ab, magni culpa similique est debitis consequatur voluptas sapiente, accusantium cupiditate quaerat autem doloremque consequuntur atque molestias numquam aliquam odio perspiciatis voluptate! Itaque, corrupti similique provident, voluptatem unde dolor enim vero deserunt porro quo quae? Doloremque cupiditate dolorem soluta! Maiores voluptas culpa eos. Ratione dolorum quidem incidunt sed maiores distinctio, perferendis dolorem? Similique possimus tenetur consequatur assumenda illo deserunt minima? Quo non magnam illum blanditiis eveniet voluptas sequi eligendi amet minima ad. Cupiditate necessitatibus nihil quos odit? Assumenda neque consequuntur, accusamus saepe harum architecto ipsum maxime!"
        ]
        ];

    public static function all()
    {
        return collect(self::$blog_posts);
    }

    public static function find($slug)
    {
        $posts = static::all();

        // $post = [];
        // foreach ($posts as $p){
        //     if($p["slug"] === $slug){
        //         $post = $p;

        //     }
        // }
        
        return $posts->firstwhere('slug', $slug);
    }
}
