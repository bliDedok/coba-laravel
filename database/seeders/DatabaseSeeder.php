<?php

namespace Database\Seeders;
use App\Models\User;
use App\Models\Category;
use App\Models\Post;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(3)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);


        User::create([
            'name' => 'Dedy Wanditya',
            'username' => 'DedyWanditya',
            'email' => 'made@gmail.com',
            'password' => bcrypt('12345')
        ]);
        // User::create([
        //     'name' => 'Sandy',
        //     'email' => 'imadesandy@gmail.com',
        //     'password' => bcrypt('67890')
        // ]);


        Category::create([
            'name' => 'Web Programming',
            'slug' => 'web-Programming'

        ]);

        Category::create([
            'name' => 'Personal',
            'slug' => 'personal'

        ]);
        Category::create([
            'name' => 'Web Design',
            'slug' => 'web-design'

        ]);

        Post::factory(20)->create();

        // Post::create([
        //     'title' => 'Judul Pertama',
        //     'slug' => 'judul-pertama',
        //     'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum asperiores dolor hic pariatur qui temporibus est eligendi nihil porro similique nulla, iusto ex magni, voluptatem esse fugit delectus, adipisci vero libero. Recusandae libero enim at animi sequi autem inventore illum nam aut. Omnis cupiditate iure porro rem similique quod voluptatem doloribus provident vero ex ipsum sapiente dolores veniam iste beatae et qui, est ab nemo, mollitia aperiam. Facilis, doloremque sed temporibus laboriosam voluptate vel voluptatibus id illum. Quo autem delectus vero at, inventore quod blanditiis cum doloremque quibusdam voluptate odit rerum illo dolor repellendus numquam consectetur temporibus, obcaecati libero natus.',
        //     'category_id' => 1,
        //     'user_id' => 1
        // ]);
        // Post::create([
        //     'title' => 'Judul Kedua',
        //     'slug' => 'judul-kedua',
        //     'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum asperiores dolor hic pariatur qui temporibus est eligendi nihil porro similique nulla, iusto ex magni, voluptatem esse fugit delectus, adipisci vero libero. Recusandae libero enim at animi sequi autem inventore illum nam aut. Omnis cupiditate iure porro rem similique quod voluptatem doloribus provident vero ex ipsum sapiente dolores veniam iste beatae et qui, est ab nemo, mollitia aperiam. Facilis, doloremque sed temporibus laboriosam voluptate vel voluptatibus id illum. Quo autem delectus vero at, inventore quod blanditiis cum doloremque quibusdam voluptate odit rerum illo dolor repellendus numquam consectetur temporibus, obcaecati libero natus.',
        //     'category_id' => 1,
        //     'user_id' => 1
        // ]);
        // Post::create([
        //     'title' => 'Judul Ketiga',
        //     'slug' => 'judul-ketiga',
        //     'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum asperiores dolor hic pariatur qui temporibus est eligendi nihil porro similique nulla, iusto ex magni, voluptatem esse fugit delectus, adipisci vero libero. Recusandae libero enim at animi sequi autem inventore illum nam aut. Omnis cupiditate iure porro rem similique quod voluptatem doloribus provident vero ex ipsum sapiente dolores veniam iste beatae et qui, est ab nemo, mollitia aperiam. Facilis, doloremque sed temporibus laboriosam voluptate vel voluptatibus id illum. Quo autem delectus vero at, inventore quod blanditiis cum doloremque quibusdam voluptate odit rerum illo dolor repellendus numquam consectetur temporibus, obcaecati libero natus.',
        //     'category_id' => 2,
        //     'user_id' => 2
        // ]);
        // Post::create([
        //     'title' => 'Judul Keempat',
        //     'slug' => 'judul-keempat',
        //     'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum asperiores dolor hic pariatur qui temporibus est eligendi nihil porro similique nulla, iusto ex magni, voluptatem esse fugit delectus, adipisci vero libero. Recusandae libero enim at animi sequi autem inventore illum nam aut. Omnis cupiditate iure porro rem similique quod voluptatem doloribus provident vero ex ipsum sapiente dolores veniam iste beatae et qui, est ab nemo, mollitia aperiam. Facilis, doloremque sed temporibus laboriosam voluptate vel voluptatibus id illum. Quo autem delectus vero at, inventore quod blanditiis cum doloremque quibusdam voluptate odit rerum illo dolor repellendus numquam consectetur temporibus, obcaecati libero natus.',
        //     'category_id' => 2,
        //     'user_id' => 2
        // ]);

    }
}
